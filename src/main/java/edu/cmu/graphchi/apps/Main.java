package edu.cmu.graphchi.apps;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;

public class Main {
	private static boolean pageRank = false, topicSensitivePR = false, weightedPR = false, distribution = false, eleminateStars = false;
	private static String graphFile = "", weightFile = "", numShards = "", numIters = "", date = "",
			outputPath = "", type = "", limit = "";
	private final static String FILETYPE = "edgelist"; //adjlist
	
	public static void main(String[] args) {
		System.out.println("Starting App");

        if (args.length > 0) {
            try {
                int i = 0;
                while (i < args.length) {
                    
                    if(args[i].startsWith("-graphFile")){
                    	graphFile = args[i].split("=")[1];
                    }
                    
                    if(args[i].startsWith("-numShards")){
                    	numShards = args[i].split("=")[1];
                    }
                    
                    if(args[i].startsWith("-numIters")){
                    	numIters = args[i].split("=")[1];
                    }
                    
                    if(args[i].startsWith("-date")){
                    	date = args[i].split("=")[1];
                    }
                    
                    if(args[i].startsWith("-type")){
                    	type = args[i].split("=")[1];
                    }
                    
                    if(args[i].startsWith("-outputPath")){
                    	outputPath = args[i].split("=")[1];
                    }
                    
                    if(args[i].startsWith("-weightFile")){
                    	weightFile = args[i].split("=")[1];
                    }
                    
                    if(args[i].startsWith("-limit")){
                    	limit = args[i].split("=")[1];
                    }
                    
                    if (args[i].startsWith("--pageRank")) {
                    	pageRank = true;
                    }
                    
                    if (args[i].startsWith("--topicSensitivePR")) {
                    	topicSensitivePR = true;
                    }
                    
                    if (args[i].startsWith("--weightedPR")) {
                    	weightedPR = true;
                    }
                    
                    if (args[i].startsWith("--distribution")) {
                    	distribution = true;
                    }
                    
                    if (args[i].startsWith("--eleminateStars")) {
                    	eleminateStars = true;
                    }
                    
                    i++;
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }
        
        if(distribution){
        	System.out.println("Edge distributions on graph...");
        	
        	if(!graphFile.equals("") && !numShards.equals("") && !date.equals("") && !outputPath.equals("") ){
        		try {
					Distribution.distribution(graphFile, numShards, "1", FILETYPE, date, outputPath);
				} catch (Exception e) {
					e.printStackTrace();
				}
        	} else
        		System.out.println("Error! Usage: --distribution -graphFile=%PATH% -numShards=#"
        				+ "-date=dd-MM-yyyy -outputPath=%PATH%");
        }
        
        if(eleminateStars){
        	System.out.println("Extracking change to follow on graph...");
        	
        	if(!graphFile.equals("") && !numShards.equals("") && !date.equals("") && !outputPath.equals("") && !limit.equals("") ){
        		try {
					EleminateStars.eleminateStars(graphFile, numShards, "1", FILETYPE, date, outputPath, Integer.parseInt(limit));
				} catch (Exception e) {
					e.printStackTrace();
				}
        	} else
        		System.out.println("Error! Usage: --eleminateStars -graphFile=%PATH% -numShards=#"
        				+ "-date=dd-MM-yyyy -outputPath=%PATH%");
        }
        
        if(pageRank){
        	System.out.println("PR on graph...");
        	
        	if(!graphFile.equals("") && !numShards.equals("") && !numIters.equals("") &&
        			!date.equals("") && !outputPath.equals("") ){
        		try {
					Pagerank.pageRank(graphFile, numShards, numIters, FILETYPE, date, outputPath);
				} catch (Exception e) {
					e.printStackTrace();
				}
        	} else
        		System.out.println("Error! Usage: --pageRank -graphFile=%PATH% -numShards=# -numIters=#"
        				+ "-date=dd-MM-yyyy -outputPath=%PATH%");
        }
        
        if(topicSensitivePR){
        	System.out.println("Topic sensitive PR on graph...");
        	
        	if(!graphFile.equals("") && !numShards.equals("") && !numIters.equals("") 
        			&& !weightFile.equals("") && !date.equals("") && !outputPath.equals("")){
        		try {
        			ArrayList<String> scoreFiles, scoreTypes;
        			String DELIM = "#";
        			String weightFileExt = ".weight.mapped";
        			int INF_INDEX = -1, FAV_INDEX = -1, RT_INDEX = -1;
    				scoreFiles = new ArrayList<String>();
    				scoreTypes = new ArrayList<String>();
        			try {
        				InputStream inputStream = new FileInputStream("scoreResources/ScoreFiles.txt");
        				Reader reader = new InputStreamReader(inputStream, "UTF8");
        				BufferedReader br = new BufferedReader(reader);
        				String line;
        				
        				while((line = br.readLine()) != null){
        					String[] values = line.split(DELIM);
        					scoreFiles.add(values[0]);
        					scoreTypes.add(values[1]);
        				}
        				br.close();
        			} catch (IOException e) {
        				e.printStackTrace();
        			}
        			for(int i = 0; i < scoreTypes.size(); i++){
        				if(scoreTypes.get(i).equals("FC"))
        					FAV_INDEX = i;
        				else if(scoreTypes.get(i).equals("R"))
        					RT_INDEX = i;
        				else if(scoreTypes.get(i).equals("I"))
        					INF_INDEX = i;
        			}
        			
        			for(int i = 0; i<scoreFiles.size(); i++){
        				if(i != FAV_INDEX && i != RT_INDEX && i != INF_INDEX)
        					TopicSensetivePR.topicSensitivePR(graphFile, numShards, numIters, 
        						FILETYPE, weightFile + File.separator + scoreFiles.get(i) + date + weightFileExt,
        						scoreFiles.get(i), outputPath, date);
        			}
				} catch (Exception e) {
					e.printStackTrace();
				}
        	} else
        		System.out.println("Error! Usage: --topicSensitivePR -graphFile=%PATH% -numShards=# "
        				+ "-numIters=# -weightFile=%PATH% -date=dd-MM-yyyy -outputPath=%PATH%");
        }
        
        if(weightedPR){
        	System.out.println("Weighted PR on graph...");
        	
        	if(!graphFile.equals("") && !numShards.equals("") && !numIters.equals("") 
        			&& !date.equals("") && !outputPath.equals("") && !type.equals("")){
        		try {
        			WeightedPagerank.weightedPR(graphFile, numShards, numIters, FILETYPE, outputPath, date, type);
        			
				} catch (Exception e) {
					e.printStackTrace();
				}
        	} else
        		System.out.println("Error! Usage: --weightedPR -graphFile=%PATH% -numShards=# "
        				+ "-numIters=# -date=dd-MM-yyyy -outputPath=%PATH% -type=%TOPIC%");
        	
        }
        
        System.out.println("App end...");
	}//main

}
