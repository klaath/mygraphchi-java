package edu.cmu.graphchi.apps;

import edu.cmu.graphchi.*;
import edu.cmu.graphchi.datablocks.FloatConverter;
import edu.cmu.graphchi.engine.GraphChiEngine;
import edu.cmu.graphchi.engine.VertexInterval;
import edu.cmu.graphchi.preprocessing.EdgeProcessor;
import edu.cmu.graphchi.preprocessing.FastSharder;
import edu.cmu.graphchi.preprocessing.VertexIdTranslate;
import edu.cmu.graphchi.preprocessing.VertexProcessor;
import edu.cmu.graphchi.util.IdFloat;
import edu.cmu.graphchi.util.Toplist;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.TreeSet;
import java.util.logging.Logger;

/**
 * Example application: PageRank (http://en.wikipedia.org/wiki/Pagerank)
 * Iteratively computes a pagerank for each vertex by averaging the pageranks
 * of in-neighbors pageranks.
 * @author akyrola
 */
public class WeightedPR implements GraphChiProgram<Float, Float> {

    private static Logger logger = ChiLogger.getLogger("pagerank");
    private static HashMap<Integer, Float> topicWeights;
    private static int niters;
    private static float totalWeights = 0;

    public void update(ChiVertex<Float, Float> vertex, GraphChiContext context)  {

    	if (context.getIteration() == 0) {
            /* Initialize on first iteration */
        	vertex.setValue(1.0f/context.getNumVertices());
        } else {
            /* On other iterations, set my value to be the weighted
               average of my in-coming neighbors pageranks.
             */
            float sum = 0.f;
            for(int i=0; i<vertex.numInEdges(); i++) {
                sum += vertex.inEdge(i).getValue();
            }
            //vertex.setValue(0.15f/context.getNumVertices() + 0.85f * sum);
            vertex.setValue(0.15f/context.getNumVertices() + 0.85f * sum);
        }

        /* Write my value (divided by my out-degree) to my out-edges so neighbors can read it. */
        float outValue = vertex.getValue() / vertex.numOutEdges();
        for(int i=0; i<vertex.numOutEdges(); i++) {
        	Float weight;
        	if(topicWeights.containsKey(vertex.outEdge(i).getVertexId()) && topicWeights.get(vertex.outEdge(i).getVertexId()) != -1.0f)
            	weight = topicWeights.get(vertex.outEdge(i).getVertexId());
        	else{
        		weight = 0f;
        	}
            vertex.outEdge(i).setValue(outValue*weight);
            
        }
        
    }


    /**
     * Callbacks (not needed for Pagerank)
     */
    public void beginIteration(GraphChiContext ctx) {}
    public void endIteration(GraphChiContext ctx) {}
    public void beginInterval(GraphChiContext ctx, VertexInterval interval) {}
    public void endInterval(GraphChiContext ctx, VertexInterval interval) {}
    public void beginSubInterval(GraphChiContext ctx, VertexInterval interval) {}
    public void endSubInterval(GraphChiContext ctx, VertexInterval interval) {}

    /**
     * Initialize the sharder-program.
     * @param graphName
     * @param numShards
     * @return
     * @throws IOException
     */
    protected static FastSharder<Float, Float> createSharder(String graphName, int numShards) throws IOException {
        return new FastSharder<Float, Float>(graphName, numShards, new VertexProcessor<Float>() {
            public Float receiveVertexValue(int vertexId, String token) {
                return (token == null ? 0.0f : Float.parseFloat(token));
            }
        }, new EdgeProcessor<Float>() {
            public Float receiveEdge(int from, int to, String token) {
                return (token == null ? 0.0f : Float.parseFloat(token));
            }
        }, new FloatConverter(), new FloatConverter());
    }

    /**
     * Usage: java edu.cmu.graphchi.demo.PageRank graph-name num-shards score-weight filetype(edgelist|adjlist)
     * For specifying the number of shards, 20-50 million edges/shard is often a good configuration.
     */
    public static void weightedPR(String graphFile, String numShards, String numIters, 
    		String fileType, String weightFile, String scoreName, String outputPath, String date) throws  Exception {
    	long startTime = System.currentTimeMillis();
        String baseFilename = graphFile;
        int nShards = Integer.parseInt(numShards);
        niters = Integer.parseInt(numIters);
        
        //Read weight file
        topicWeights = new HashMap<Integer, Float>();
        if(readWeights(weightFile) != -1){
        
	        /* Create shards */
	        FastSharder<Float, Float> sharder = createSharder(baseFilename, nShards);
	        if (baseFilename.equals("pipein")) {     // Allow piping graph in
	            sharder.shard(System.in, fileType);
	        } else {
	            if (!new File(ChiFilenames.getFilenameIntervals(baseFilename, nShards)).exists()) {
	                sharder.shard(new FileInputStream(new File(baseFilename)), fileType);
	            } else {
	                logger.info("Found shards -- no need to preprocess");
	            }
	        }
	
	        /* Run GraphChi */
	        GraphChiEngine<Float, Float> engine = new GraphChiEngine<Float, Float>(baseFilename, nShards);
	        engine.setEdataConverter(new FloatConverter());
	        engine.setVertexDataConverter(new FloatConverter());
	        engine.setModifiesInedges(false); // Important optimization
	        //Run job
	        engine.run(new TopicSensetivePR(), niters);
	
	        logger.info("Ready.");
	        long endTime   = System.currentTimeMillis();
	        long totalTime = endTime - startTime;
	        System.out.println("Pagerank Running Time: " + totalTime);
	        /* Output results */
	        VertexIdTranslate trans = engine.getVertexIdTranslate();
	        TreeSet<IdFloat> resultSet = Toplist.topListFloat(baseFilename, engine.numVertices(), 3500000);
	        writeResults(outputPath + File.separator + scoreName + date + ".score"
	        		, trans, resultSet);
	        endTime   = System.currentTimeMillis();
	        totalTime = endTime - startTime;
	        System.out.println("Writing Time: " + totalTime);
        }
    }
    
    public static void writeResults(String outFile, VertexIdTranslate trans, TreeSet<IdFloat> resultSet){
    	
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(outFile));
            BufferedWriter bw1 = new BufferedWriter(new FileWriter(outFile + ".listweights"));
            for(IdFloat vertexRank : resultSet) {
            	
            	if(topicWeights.containsKey(vertexRank.getVertexId())){
            		bw.write(trans.backward(vertexRank.getVertexId()) + "\t" + vertexRank.getValue() + "\n");
            		bw1.write(trans.backward(vertexRank.getVertexId()) + "\t" + topicWeights.get(vertexRank.getVertexId()) +"\n");
            	}
            }
            bw.close();
            bw1.close();
            System.out.println("PGScores are written into " + outFile);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private  static int readWeights(String fileName){
    	try {
			BufferedReader br = new BufferedReader(new FileReader(new File(fileName)));
			String line;
			while((line = br.readLine()) != null){
				String[] values = line.split("\t");
				try{
					topicWeights.put(Integer.parseInt(values[0]), Float.parseFloat(values[1]));
					if(Float.parseFloat(values[1]) != -1.0f)
						totalWeights += Float.parseFloat(values[1]);
				}catch (NumberFormatException e) {
					e.printStackTrace();
					continue;
				}
			}
			br.close();
			return 1;
		} catch (FileNotFoundException e) {
			//e.printStackTrace();
			System.out.println("No weight file");
			return -1;
		} catch (IOException e) {
			e.printStackTrace();
			return -1;
		}
    }
}
