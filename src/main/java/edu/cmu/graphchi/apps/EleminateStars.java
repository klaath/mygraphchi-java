package edu.cmu.graphchi.apps;

import edu.cmu.graphchi.*;
import edu.cmu.graphchi.datablocks.FloatConverter;
import edu.cmu.graphchi.engine.GraphChiEngine;
import edu.cmu.graphchi.engine.VertexInterval;
import edu.cmu.graphchi.io.CompressedIO;
import edu.cmu.graphchi.preprocessing.EdgeProcessor;
import edu.cmu.graphchi.preprocessing.FastSharder;
import edu.cmu.graphchi.preprocessing.VertexProcessor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Logger;


public class EleminateStars implements GraphChiProgram<Float, Float> {

    private static Logger logger = ChiLogger.getLogger("distribution");
    private static ArrayList<Integer> filtered = new ArrayList<Integer>();
    private static int limit = 100;
    
    public void update(ChiVertex<Float, Float> vertex, GraphChiContext context)  {
        if (context.getIteration() == 0) {
        	if(vertex.numOutEdges() > limit)
        		filtered.add(vertex.getId());
        }

    }


    /**
     * Callbacks (not needed for Pagerank)
     */
    public void beginIteration(GraphChiContext ctx) {}
    public void endIteration(GraphChiContext ctx) {}
    public void beginInterval(GraphChiContext ctx, VertexInterval interval) {}
    public void endInterval(GraphChiContext ctx, VertexInterval interval) {}
    public void beginSubInterval(GraphChiContext ctx, VertexInterval interval) {}
    public void endSubInterval(GraphChiContext ctx, VertexInterval interval) {}

    /**
     * Initialize the sharder-program.
     * @param graphName
     * @param numShards
     * @return
     * @throws IOException
     */
    protected static FastSharder createSharder(String graphName, int numShards) throws IOException {
        return new FastSharder<Float, Float>(graphName, numShards, new VertexProcessor<Float>() {
            public Float receiveVertexValue(int vertexId, String token) {
                return (token == null ? 0.0f : Float.parseFloat(token));
            }
        }, new EdgeProcessor<Float>() {
            public Float receiveEdge(int from, int to, String token) {
                return (token == null ? 0.0f : Float.parseFloat(token));
            }
        }, new FloatConverter(), new FloatConverter());
    }

    /**
     * Usage: java edu.cmu.graphchi.demo.PageRank graph-name num-shards filetype(edgelist|adjlist)
     * For specifying the number of shards, 20-50 million edges/shard is often a good configuration.
     */
    public static void eleminateStars(String graphFile, String numShards, String numIters, 
    		String fileType, String date, String outputPath, int edgeLimit) throws  Exception {
    	long startTime = System.currentTimeMillis();
        String baseFilename = graphFile;
        int nShards = Integer.parseInt(numShards);
        int niters = Integer.parseInt(numIters);
        limit = edgeLimit;
        CompressedIO.disableCompression();

        /* Create shards */
        FastSharder sharder = createSharder(baseFilename, nShards);
        if (baseFilename.equals("pipein")) {     // Allow piping graph in
            sharder.shard(System.in, fileType);
        } else {
            if (!new File(ChiFilenames.getFilenameIntervals(baseFilename, nShards)).exists()) {
                sharder.shard(new FileInputStream(new File(baseFilename)), fileType);
            } else {
                logger.info("Found shards -- no need to preprocess");
            }
        }

        /* Run GraphChi */
        GraphChiEngine<Float, Float> engine = new GraphChiEngine<Float, Float>(baseFilename, nShards);
        engine.setEdataConverter(new FloatConverter());
        engine.setVertexDataConverter(new FloatConverter());
        engine.setModifiesInedges(false); // Important optimization

        engine.run(new EleminateStars(), niters);

        logger.info("Ready.");
        long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println("Pagerank Running Time: " + totalTime);
        /* Output results */
        writeResults(outputPath + File.separator + date + ".nonStars");
        endTime   = System.currentTimeMillis();
        totalTime = endTime - startTime;
        System.out.println("Writing Time: " + totalTime);
    }
    
    public static void writeResults(String outFile){
    	
        try {
        	OutputStream output = new FileOutputStream(outFile);
        	OutputStreamWriter writer = new OutputStreamWriter(output);
            BufferedWriter bw = new BufferedWriter(writer);
            
            for(int  id : filtered) {
                bw.write(id + "\n");
            }
            bw.close();
            System.out.println("Edge distributions are written into " + outFile);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

